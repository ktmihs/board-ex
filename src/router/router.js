import { logGuard, userGuard } from '@/middlewares/routerGuard';
import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'Index',
		component: () => import('@/views/IndexPage.vue'),
	},
	{
		path: '/detail/:id',
		name: 'Detail',
		component: () => import('@/views/DetailPage.vue'),
	},
	{
		path: '/post',
		name: 'Post',
		component: () => import('@/views/PostPage.vue'),
		beforeEnter: userGuard,
	},
	{
		path: '/signin',
		name: 'Signin',
		component: () => import('@/views/SigninPage.vue'),
		beforeEnter: logGuard,
	},
	{
		path: '/signup',
		name: 'Signup',
		component: () => import('@/views/SignupPage.vue'),
		beforeEnter: logGuard,
	},
	{
		path: '/mypage',
		name: 'MyPage',
		component: () => import('@/views/MyPage.vue'),
		beforeEnter: userGuard,
	},
	{
		path: '*',
		name: '404',
		component: () => import('@/views/404.vue'),
	},
];

const router = new VueRouter({
	mode: 'history',
	routes,
});

export default router;
