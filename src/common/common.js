const checkAllInput = userInfo =>
	Object.values(userInfo).every(value => !!value);
export { checkAllInput };
