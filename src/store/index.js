import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex);

export const store = new Vuex.Store({
	state: {
		contacts: [],
		contact: {},
	},
	getters: {
		getContacts(state) {
			return state.contacts;
		},
		getContact(state) {
			return state.contact;
		},
	},
	mutations: {
		setContacts(state, data) {
			state.contacts = data.map(dataset => {
				const { id, name, username, email, address, phone, website } = dataset;
				const { city } = address;
				return { id, name, username, email, city, phone, website };
			});
		},
		setOneContact(state, data) {
			if (data.address) {
				const { id, name, username, email, address, phone, website } = data;
				const { city } = address;
				state.contact = { id, name, username, email, city, phone, website };
			} else state.contact = data;
		},

		postOneContact(state, user) {
			if (!user) return false;
			user.id =
				state.contacts.reduce((max, data) => Math.max(max, data.id), 0) + 1;
			state.contacts.push(user);
		},
		deleteOneContact(state, id) {
			if (!id) return false;
			state.contacts = state.contacts.filter(user => user.id !== id);
		},
	},
	actions: {
		getAxiosContacts({ commit }) {
			return axios
				.get('https://jsonplaceholder.typicode.com/users')
				.then(({ data }) => {
					commit('setContacts', data);
				})
				.catch(() => commit('setContacts', []));
		},
		getOneContact({ commit, state }, id) {
			return axios
				.get(`https://jsonplaceholder.typicode.com/users/${id}`)
				.then(({ data }) => {
					commit('setOneContact', data);
				})
				.catch(() => {
					commit(
						'setOneContact',
						state.contacts.filter(contact => contact.id === id)[0]
					);
				});
		},
	},
});
