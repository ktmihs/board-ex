export const logGuard = (to, from, next) => {
	if (localStorage.getItem('hasLog')) {
		const logout = confirm(
			'이미 로그인 된 사용자입니다. 로그아웃을 하시겠습니까?'
		);
		if (logout) {
			localStorage.removeItem('hasLog');
			next('/');
		} else next(from.path);
	} else next();
};

export const userGuard = (to, from, next) => {
	if (!localStorage.getItem('hasLog')) {
		next('/');
		alert('로그인이 필요한 페이지 입니다.');
	} else next();
};
