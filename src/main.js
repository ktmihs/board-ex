import Vue from 'vue';
import App from './App.vue';
import router from './router/router';
import { store } from './store';
import { Plugin } from 'vue-fragment';
// eslint-disable-next-line no-unused-vars
import style from './style.css';

Vue.config.productionTip = false;
Vue.use(Plugin);

new Vue({
	router,
	store,
	render: h => h(App),
}).$mount('#app');
